<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Preprocessing
            <small>Analisis Sentimen </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo base_url('admin_preprocessing') ?>"><i>Preprocessing</i></a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php 
									if($this->session->flashdata('message') != null) 
									{ 
									echo '<div class="alert alert-'.$this->session->flashdata('type').'" role="alert" style="margin-inline-start: 12px;margin-inline-end: 12px;">'; 
                                    echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'; 
                                    echo '<i class="icon fa fa-check"></i>';
									echo $this->session->flashdata('message') <> '' ? $this->session->flashdata('message') : ''; 
									echo '</div>'; 
									}
								?>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">

                        <form action="" method="post">
                            <div class="row">
                                <!-- /.col -->
                                <div class="col-xs-2">
                                    <button id="btn" onclick="button_spin()" type="submit" class="btn btn-success btn-block bg-green"><i class="glyphicon glyphicon-tasks"></i> Preprocessing</button>
                                </div>
                                <!-- /.col -->
                            </div>
                        </form>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Original</th>
                                    <th>Filtered</th>
                                    <th>Stemmed</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>