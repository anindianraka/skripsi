<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_login extends CI_Controller
{
    public function index(){
        $data['content'] = NULL;
        
        if ($this->session->userdata('ses_admin') == true){
			redirect(base_url('admin_dashboard'));
        }
        else{
            $this->load->view('admin/login');
        }
    }
    public function login(){

		$username= $this->input->post("username");
		$password= $this->input->post("password");
		
		//cek input validation
		$this->form_validation->set_rules('username','Username','trim|required|max_length[40]');
		$this->form_validation->set_rules('password','Password','trim|required|max_length[40]');
		if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('message','Input Nama Pengguna atau Kata Sandi tidak valid!');
            $this->session->set_flashdata('type','danger');
            redirect('/');
        }
        else{    
            $statuslogin = $this->model_login->checklogin($username, $password);			
			if(count($statuslogin)>=1){
                //var_dump($statuslogin);
                $this->session->set_userdata('ses_admin', array('id' => $statuslogin[0]['id_admin'],
                                                                'nama' => $statuslogin[0]['username'],
																'akses' => $statuslogin[0]['akses']                                                                
                                                                ));
                if ($statuslogin[0]['akses']=='admin') {
                    redirect (base_url('admin_dashboard'));
                } else {
                    redirect (base_url('admin_twitter'));
                }
                
                    
					
            }
            else{
				$this->session->set_flashdata('message','Nama Pengguna atau Kata Sandi salah!');
				$this->session->set_flashdata('type','danger');
				redirect('/');
			}
        }
        redirect('/');
		
			
	}
    public function logout(){
        $this->session->sess_destroy('ses_admin'); 
        redirect(base_url('/'));
    }
}
