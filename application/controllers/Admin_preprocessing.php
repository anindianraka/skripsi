<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_preprocessing extends CI_Controller
{
    public function index(){
        $data['content'] = NULL;
        $sess = $this->session->userdata('ses_admin');
        $data['page']= "preprocessing";
        $this->load->view('admin/layout',$data);
    }
    public function selectdata(){
        $hasil = $this->model_ekstraksi->selectdata();
        return $hasil;
    }
    public function proses(){
        $select = $this->selectdata();
        $this->model_ekstraksi->insertterm($select);
        $this->session->set_flashdata('message','Berhasil melakukan preprocessing!');
		$this->session->set_flashdata('type','success');
        //redirect (base_url('admin_preprocessing'));
        echo json_encode(array("status" => TRUE));
    }
    
    public function ajax_list(){
    	$list=$this->model_preprocessing->get_datatables();
    	$data=array();
    	$no= $_POST['start'];
    	foreach ($list as $admin_preprocessing) {
    		$no++;
    		$row = array();
            $row[]=$no;
            $row[]=$admin_preprocessing->term_tokenized;
            $row[]=$admin_preprocessing->term_filtered;
            $row[]=$admin_preprocessing->term_stemmed;

    		$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_obat('."'".$admin_preprocessing->id_bagofwords."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$admin_preprocessing->id_bagofwords."'".')"><i class="glyphicon glyphicon-trash"></i> Delete </a>';

            $data[] = $row;
    	}
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_preprocessing->count_all(),
                        "recordsFiltered" => $this->model_preprocessing->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
}
