<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Latih
            <small>Analisis Sentimen</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo base_url('admin_datalatih') ?>">Data Latih</a></li>
            <!-- <li class="active">Data Obat</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php 
		if($this->session->flashdata('message') != null) 
        { 
        echo '<div class="alert alert-'.$this->session->flashdata('type').'" role="alert" style="margin-inline-start: 12px;margin-inline-end: 12px;">'; 
        echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'; 
        echo '<i class="icon fa fa-check"></i>';
        echo $this->session->flashdata('message') <> '' ? $this->session->flashdata('message') : ''; 
        echo '</div>'; 
        }
    ?>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="col-xs-2">
                            <form action="" method="post" id="form_latih">
                                <div class="input-group margin">
                                    <div class="input-group-btn">
                                        <button type="button" onclick="button_spin()" class="btn btn-success bg-green"><i
                                                class="glyphicon glyphicon-repeat"></i> Latih</button>
                                    </div>
                                    <input type="number" name="k" class="form-control" placeholder="K" id="input_k">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tweet</th>
                                    <th>Prediksi Eucledian</th>
                                    <th>Prediksi Cosine</th>
                                    <th>Prediksi Jaccord</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

<!-- modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Person Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id" />
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Obat</label>
                            <div class="col-md-9">
                                <input name="namaObat" placeholder="Nama Obat" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Stok Obat</label>
                            <div class="col-md-9">
                                <input name="stokObat" placeholder="Stok Obat" class="form-control" type="number">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kemasan Obat</label>
                            <div class="col-md-9">
                                <select name="kemasanObat" class="form-control">
                                    <option value="Kaplet">Kaplet</option>
                                    <option value="Pack">Pack</option>
                                    <option value="Toples">Toples</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->