<script src="<?php echo base_url('assets/admin/bower_components/jquery/dist/jquery.min.js'); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('assets/admin/bower_components/jquery-ui/jquery-ui.min.js');?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/plugins/iCheck/all.css');?>">
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/admin/bower_components/raphael/raphael.min.js');?>"></script>
<script src="<?php echo base_url('assets/admin/bower_components/morris.js/morris.min.js');?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js');?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
<script src="<?php echo base_url('assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/admin/bower_components/jquery-knob/dist/jquery.knob.min.js');?>"></script>
<!-- daterangepicker --><!-- 
<script src="assets/admin/bower_components/moment/min/moment.min.js"></script>
<script src="assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<!-- datepicker -->
<!-- <script src="assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> -->
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js');?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/admin/bower_components/fastclick/lib/fastclick.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/admin/dist/js/adminlte.min.js');?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="assets/admin/dist/js/pages/dashboard.js"></script>
AdminLTE for demo purposes
<script src="assets/admin/dist/js/demo.js"></script> -->
<!-- DataTables -->
<script src="<?php echo base_url('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_url('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/swal/js/sweetalert2.all.min.js');?>"></script>
<!-- ChartJS -->
<script src="<?php echo base_url('assets/admin/bower_components/chart.js/Chart.js');?>"></script>
<script type="text/javascript">

var save_method; //for save method string
var table;


$(document).ready(function() {

	

    $('#matrix').hide();
    $("#btn_akurasi").click(function(){
        loadmatrix();
        reload_table();
    });
    $('#matrix_eucledian').hide();
    $("#btn_akurasi_eucledian").click(function(){
        loadmatrix_eucledian();
        reload_table();
    });
    $('#matrix_jaccord').hide();
    $("#btn_akurasi_jaccord").click(function(){
        loadmatrix_jaccord();
        reload_table();
    });
    $('#matrix_cosine').hide();
    $("#btn_akurasi_cosine").click(function(){
        loadmatrix_cosine();
        reload_table();
    });
	jQuery('ul .tabs li').on('click', function (e) {
		var currentAttrValue = jQuery(this).attr('href');
		// Show/Hide Tabs
		$('ul.tabs li').removeClass('active');
		$('.tab-pane').removeClass('active');

		$(this).addClass('active');
		$("#"+currentAttrValue).addClass('active');
	});
    //datatables
    table = $('#example1').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url($this->uri->segment(1).'/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

	});
	//-------------
	//- PIE CHART -
	//-------------
	// Get context with jQuery - using jQuery's .get() method.
	var data_positif = $("#jumlah-data-positif").text();
    var data_negatif = $("#jumlah-data-negatif").text();
	var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
	var pieChart = new Chart(pieChartCanvas)
	var PieData = [{
			value: data_negatif,
			color: '#f56954',
			highlight: '#f56954',
			label: 'Negatif'
		},
		{
			value: data_positif,
			color: '#00a65a',
			highlight: '#00a65a',
			label: 'Positif'
		}
	]
	var pieOptions = {
		//Boolean - Whether we should show a stroke on each segment
		segmentShowStroke: true,
		//String - The colour of each segment stroke
		segmentStrokeColor: '#fff',
		//Number - The width of each segment stroke
		segmentStrokeWidth: 2,
		//Number - The percentage of the chart that we cut out of the middle
		percentageInnerCutout: 50, // This is 0 for Pie charts
		//Number - Amount of animation steps
		animationSteps: 100,
		//String - Animation easing effect
		animationEasing: 'easeOutBounce',
		//Boolean - Whether we animate the rotation of the Doughnut
		animateRotate: true,
		//Boolean - Whether we animate scaling the Doughnut from the centre
		animateScale: false,
		//Boolean - whether to make the chart responsive to window resizing
		responsive: true,
		// Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
		maintainAspectRatio: true,
		//String - A legend template
		legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
	}
	//Create pie or douhnut chart
	// You can switch between pie and douhnut using the method below.
	pieChart.Doughnut(PieData, pieOptions)
});
function loadmatrix_eucledian(){
	$.ajax({
		url:"<?php echo base_url().'Admin_akurasi_eucledian/matrix_akurasi'?>",
		dataType: "json",
		success: function(data){
            // console.log(data);
			print_matrix_contents(data);
            $('#matrix_eucledian').show();
		},
		error: function(){
			alert("Tidak dapat mengambil matriks");
		}
	});
}
function loadmatrix_jaccord(){
	$.ajax({
		url:"<?php echo base_url().'Admin_akurasi_jaccord/matrix_akurasi'?>",
		dataType: "json",
		success: function(data){
            // console.log(data);
			print_matrix_contents(data);
            $('#matrix_jaccord').show();
		},
		error: function(){
			alert("Tidak dapat mengambil matriks");
		}
	});
}
function loadmatrix_cosine(){
	$.ajax({
		url:"<?php echo base_url().'Admin_akurasi_cosine/matrix_akurasi'?>",
		dataType: "json",
		success: function(data){
            // console.log(data);
			print_matrix_contents(data);
            $('#matrix_cosine').show();
		},
		error: function(){
			alert("Tidak dapat mengambil matriks");
		}
	});
}
function print_matrix_contents(matrix){
	$("#total-datauji").append(matrix[0]); //total data uji
	$("#true-positives").append(matrix[1]); //true positives
	$("#true-negatives").append(matrix[2]); //true negatives
	$("#false-positives").append(matrix[3]); //false positives
	$("#false-negatives").append(matrix[4]); //false negatives
	$("#akurasi").append(matrix[5]*100); //akurasi
	$("#ppv").append(matrix[6]*100); //positive predictive value
	$("#sensitivity").append(matrix[7]*100); //sensitivity
	
	$("#akurasi-percentage").append((matrix[5]*100).toFixed(2)); //error rate
	
}
function button_spin(){
    $('#btn').attr('disabled',true); //set button disable
	$.ajax({
        url : "<?php echo base_url($this->uri->segment(1).'/proses')?>",
		data: $('#form_latih').serialize(),
        type: "POST",
        dataType: "JSON",

		beforeSend: function () {
			Swal.fire({
				title: 'Menunggu',
				html: 'Memproses data',
				onOpen: () => {
					swal.showLoading()
				}
			})
		},
        success: function(data)
        {
            if(data.status)
            {
				Swal.fire({
				type: 'success',
				title: 'Berhasil',
				text: 'Data berhasil di proses'
				})
                reload_table();
            }
            $('#btn').attr('disabled',false); //set button enable
        }
    });
}
function button_spin_twitter(){
    $('#btn').attr('disabled',true); //set button disable
	$.ajax({
        url : "<?php echo base_url($this->uri->segment(1).'/proses')?>",
		data: $('#form_latih').serialize(),
        type: "POST",
        dataType: "JSON",

		beforeSend: function () {
			Swal.fire({
				title: 'Menunggu',
				html: 'Memproses data',
				onOpen: () => {
					swal.showLoading()
				}
			})
		},
        success: function(data)
        {
            if(data.status)
            {
				Swal.fire({
				type: 'success',
				title: 'Berhasil',
				text: 'Data berhasil di proses'
				})
            }
            $('#btn').attr('disabled',false); //set button enable
			location.reload();
        }
    });
}
function delete_all(){

	Swal.fire({
		title: 'Apakah anda yakin?',
		text: "Akan menghapus data ini",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Ya, hapus ini!'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: "<?php echo base_url($this->uri->segment(1).'/delete')?>",
				type: "POST",
				dataType: "JSON",

				success: function (data) {
					if (data.status) {
						Swal.fire(
							'Deleted!',
							'Your file has been deleted.',
							'success'
						)
						reload_table();
					}
				}
			});
			
		}
	})
	
}
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax
}
function close()
{
    $('#tabelDetail').empty();
}

$('#import_csv').on('submit', function(event){
		event.preventDefault();
		$.ajax({
			url:"<?php echo base_url(); ?>admin_dataset/import",
			method:"POST",
			data:new FormData(this),
			contentType:false,
			cache:false,
			processData:false,
			beforeSend:function(){
				$('#import_csv_btn').html('Importing...');
			},
			success:function(data)
			{
				$('#import_csv')[0].reset();
				$('#import_csv_btn').attr('disabled', false);
				$('#import_csv_btn').html('Import Done');
				reload_table();
			}
		})
    });
$('#import_csv_twitter').on('submit', function(event){
		event.preventDefault();
		$.ajax({
			url:"<?php echo base_url(); ?>admin_twitter/import",
			method:"POST",
			data:new FormData(this),
			contentType:false,
			cache:false,
			processData:false,
			beforeSend:function(){
				$('#import_csv_btn').html('Importing...');
			},
			success:function(data)
			{
				$('#import_csv_twitter')[0].reset();
				$('#import_csv_btn').attr('disabled', false);
				$('#import_csv_btn').html('Import Done');
				reload_table();
			}
		})
    });

</script>