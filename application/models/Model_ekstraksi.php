<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use markfullmer\porter2\Porter2;
class Model_ekstraksi extends CI_Model {
    function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
	private $arraykatadasar = array();
	private $arraytoken = array();
	private $arrayfiltered = array();
	
	private $arraycoba = array();

	/*------------cleansing------------*/
	public function cleansing($tweet){
		$r_url = preg_replace('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $tweet);
        $re = '/#\S+\s*/'; 
        $r_hastaghs = preg_replace($re, '', $r_url);
        $r_username = preg_replace('/(\s+|^)@\S+/', '', $r_hastaghs);
        $lowercase = strtolower($r_username);
		$tokens = preg_replace('/\s+/', ' ', $lowercase);
		$tokens = preg_replace('/[^a-z \-]/','', $tokens);
		$tokens2 =  preg_replace('/[^\p{L}\p{N}\s]/u', '', $tokens);
		return $tokens2;
	}
	public function casefolding ($tweet){
        $lowercase = strtolower($r_username);
		return $lowercase;
	}

	/*------------FILTERING------------*/
	public function filtering($hasiltoken){
		//ubah string ke array
		$this->arraytoken = explode(" ",$hasiltoken);
		$commonWords = array('a','able','about','above','abroad','according','accordingly','across'
		,'actually','adj','after','afterwards','again','against','ago','ahead','aint','all','allow'
		,'allows','almost','alone','along','alongside','already','also','although','always','am'
		,'amid','amidst','among','amongst','an','and','another','any','anybody','anyhow','anyone'
		,'anything','anyway','anyways','anywhere','apart','appear','appreciate','appropriate','are'
		,'arent','around','as','as','aside','ask','asking','associated','at','available','away','awfully'
		,'b','back','backward','backwards','be','became','because','become','becomes','becoming','been'
		,'before','beforehand','begin','behind','being','believe','below','beside','besides','best','better'
		,'between','beyond','both','brief','but','by','c','came','can','cannot','cant','cant','caption','cause'
		,'causes','coz','certain','certainly','changes','clearly','cmon','co','co.','com','come','comes','concerning'
		,'consequently','consider','considering','contain','containing','contains','corresponding','could'
		,'couldnt','course','cs','currently','d','dare','darent','definitely','described','despite','did'
		,'didnt','different','directly','do','does','doesnt','doing','done','dont','down','downwards','during'
		,'e','each','edu','eg','eight','eighty','either','else','elsewhere','end','ending','enough','entirely'
		,'especially','et','etc','even','ever','evermore','every','everybody','everyone','everything'
		,'everywhere','ex','exactly','example','except','f','fairly','far','farther','few','fewer','fifth'
		,'first','five','followed','following','follows','for','forever','former','formerly','forth','forward'
		,'found','four','from','further','furthermore','g','get','gets','getting','given','gives','go','goes'
		,'going','gone','got','gotten','greetings','h','had','hadnt','half','happens','hardly','has','hasnt'
		,'have','havent','having','he','hed','hell','hello','help','hence','her','here','hereafter','hereby'
		,'herein','heres','hereupon','hers','herself','hes','hi','him','himself','his','hither','hopefully'
		,'how','howbeit','however','hundred','i','id','ie','if','ignored','ill','im','immediate','in','inasmuch'
		,'inc','inc.','indeed','indicate','indicated','indicates','inner','inside','insofar','instead','into'
		,'inward','is','isnt','it','itd','itll','its','its','itself','ive','j','just','k','keep','keeps','kept'
		,'know','known','knows','l','last','lately','later','latter','latterly','least','less','lest','let'
		,'lets','like','liked','likely','likewise','little','look','looking','looks','low','lower','ltd','m'
		,'made','mainly','make','makes','many','may','maybe','maynt','me','mean','meantime','meanwhile','merely'
		,'might','mightnt','mine','minus','miss','more','moreover','most','mostly','mr','mrs','much','must'
		,'mustnt','my','myself','n','name','namely','nd','near','nearly','necessary','need','neednt','needs'
		,'neither','never','neverf','neverless','nevertheless','new','next','nine','ninety','no','nobody','non'
		,'none','nonetheless','noone','no-one','nor','normally','not','nothing','notwithstanding','novel','now'
		,'nowhere','o','obviously','of','off','often','oh','ok','okay','old','on','once','one','ones','ones'
		,'only','onto','opposite','or','other','others','otherwise','ought','oughtnt','our','ours','ourselves'
		,'out','outside','over','overall','own','p','particular','particularly','past','per','perhaps','placed'
		,'please','plus','possible','presumably','probably','provided','provides','q','que','quite','qv','r'
		,'rather','rd','re','really','reasonably','recent','recently','regarding','regardless','regards'
		,'relatively','respectively','right','round','s','said','same','saw','say','saying','says','second'
		,'secondly','see','seeing','seem','seemed','seeming','seems','seen','self','selves','sensible','sent'
		,'serious','seriously','seven','several','shall','shant','she','shed','shell','shes','should','shouldnt'
		,'since','six','so','some','somebody','someday','somehow','someone','something','sometime','sometimes'
		,'somewhat','somewhere','soon','sorry','specified','specify','specifying','still','sub','such','sup'
		,'sure','t','take','taken','taking','tell','tends','th','than','thank','thanks','thanx','that','thatll'
		,'thats','thats','thatve','the','their','theirs','them','themselves','then','thence','there','thereafter'
		,'thereby','thered','therefore','therein','therell','therere','theres','theres','thereupon','thereve'
		,'these','they','theyd','theyll','theyre','theyve','thing','things','think','third','thirty','this'
		,'thorough','thoroughly','those','though','three','through','throughout','thru','thus','till','to'
		,'together','too','took','toward','towards','tried','tries','truly','try','trying','ts','twice','two'
		,'u','un','under','underneath','undoing','unfortunately','unless','unlike','unlikely','until','unto'
		,'up','upon','upwards','us','use','used','useful','uses','using','usually','v','value','various'
		,'versus','very','via','viz','vs','w','want','wants','was','wasnt','way','we','wed','welcome','well'
		,'well','went','were','were','werent','weve','what','whatever','whatll','whats','whatve','when','whence'
		,'whenever','where','whereafter','whereas','whereby','wherein','wheres','whereupon','wherever','whether'
		,'which','whichever','while','whilst','whither','who','whod','whoever','whole','wholl','whom','whomever'
		,'whos','whose','why','will','willing','wish','with','within','without','wonder','wont','would','wouldnt'
		,'x','y','yes','yet','you','youd','youll','your','youre','yours','yourself','yourselves','youve','z'
		,'zero');
		$this->arrayfiltered = array_diff($this->arraytoken,$commonWords);
		//ubah hasil filter ke string
		$hasilfilter = implode(" ",$this->arrayfiltered);
		return $hasilfilter;
	}
	
    public function selectdata()
    {
        $this->db->select('id_dataset,text,kategori');
		$this->db->from('sa_dataset');
        return $this->db->get()->result_array();
	}
	public function stemming($text) {
		// Ambil per kata
		$words = preg_split('/(' . "[^a-zA-Z']+" . '+)/', $text, -1, PREG_SPLIT_DELIM_CAPTURE);
			if (!count($words)) {
			return $text;
			}
			
		$isword = !preg_match('/' . "[^a-zA-Z']+" . '/', $words[0]);
		foreach ($words as $k => $word) {
			if ($isword) {
				$words[$k] = Porter2::stem($word);
			}
			$isword = !$isword;
		}
		return implode('', $words);
	}
    public function insertterm($select)
    {
		$this->db->truncate('sa_bagofwords');
		$i=0;
        foreach($select as $row){
			$text = $select[$i]['text'];
			$sentence2 = $this->model_ekstraksi->cleansing($text);
			$sentence3= $this->model_ekstraksi->filtering($sentence2);
			$output   = $this->model_ekstraksi->stemming($sentence3);
			//echo $output . "\n";
			//insert ke database
			// var_dump($output);
			$this->db->insert('sa_bagofwords',['bag_id_dataset'=>$select[$i]['id_dataset'],'term_tokenized'=>$text,'term_filtered'=>$sentence3,'term_stemmed'=>$output,'kategori_bag'=>$select[$i]['kategori']]);   
			$i++;
    	}
	}
	
}