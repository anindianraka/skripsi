<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_twitter_hasil extends CI_Model {
    function __construct(){
        parent::__construct();
        $this->load->database();
    }
    public function get_all_data_uji(){
		$this->db->select('term_stemmed');
		$this->db->from('sa_bagofwords_twitter');
        $this->db->where('id_admin', $this->session->userdata['ses_admin']['id']);
		$this->db->join('sa_dataset_twitter', 'sa_dataset_twitter.id_dataset_twitter = sa_bagofwords_twitter.bag_id_dataset_twitter');
		$array_terms = $this->db->get()->result_array();
		return $array_terms;
    }
    public function get_all_data(){
		$this->db->select('*');
		$this->db->from('sa_dataset_twitter');
        $this->db->where('id_admin', $this->session->userdata['ses_admin']['id']);
		$this->db->join('sa_bagofwords_twitter', 'sa_bagofwords_twitter.bag_id_dataset_twitter = sa_dataset_twitter.id_dataset_twitter');
		$array_terms = $this->db->get()->result_array();
		return $array_terms;
    }
    public function tfidficf_latih(){
        $kata = $this->model_latih->vocabulary();
		$vocab_count=count($kata);
		for ($i=0; $i < $vocab_count; $i++) { 
			$array_term[] = array(
				"term" => $kata[$i],
				"df" => 0,
				"cf" => 0,
				"idf" => 0,
				"icf" => 0
			);
		}        
		$array_kalimat = $this->model_latih->get_all_data_latih();
		// HITUNG DF DATA LATIH
		foreach ($array_kalimat as $row2) {
            $row2_unique= $this->model_latih->get_term($row2);
            
			for ($i=0; $i < $vocab_count; $i++) {
				foreach($row2_unique as $term2){
					if($array_term[$i]["term"]==$term2){
						$array_term[$i]["df"]++;
					}
				}
			}
		}
		///HITUNG CF DATA LATIH POSITIF
        $array_term_latih_positif = $this->model_latih->all_terms_traindata_positif();
        
		foreach ($array_term_latih_positif as $row2) {
			$row2_unique= $this->model_latih->get_term($row2);
			for ($i=0; $i < $vocab_count; $i++) {
				foreach($row2_unique as $term2){
					if($array_term[$i]["term"]==$term2){
						if($array_term[$i]["cf"]==0){
							$array_term[$i]["cf"]++;
						}
					}
				}
			}
		}
		///HITUNG CF DATA LATIH NEGATIF
		$term_gabungan = array();
		$array_term_latih_negatif = $this->model_latih->all_terms_traindata_negatif();
		foreach ($array_term_latih_negatif as $row2) {
			$row2 = implode(" ",$row2);
			$row2 = preg_replace('/\s+/', ' ', $row2);
			$row2 = trim($row2);
			$row2 = explode(" ",$row2);
			foreach ($row2 as $term) {
				array_push($term_gabungan,$term);
			}
		}
		$term_gabungan_unique = array_unique($term_gabungan);
		for ($i=0; $i < $vocab_count; $i++) {
			foreach($term_gabungan_unique as $term2){
				if($array_term[$i]["term"]==$term2){
					if($array_term[$i]["cf"]==0||$array_term[$i]["cf"]==1){
						$array_term[$i]["cf"]++;
					}
				}
			}
		}
		
		return $array_term;
	}
	
	public function tfidficf($array_uji,$k,$data_model){
		$array_label_latih=$this->model_latih->get_label_latih();
		$array_term = $data_model;
		$array_hasil_cosine=array();
		$count= count($array_term);
		$array_term_copy = $array_term;
		$array_uji_term = $this->model_latih->get_all_term($array_uji);

		foreach ($array_uji_term as $row2) {
			$array_kata=array();
			$ketemu=0;
			for ($i=0; $i < $count; $i++) { 
				///JIKA TERDAPAT DALAM ARRAY MAKA DF DALAM KATA TERSEBUT BERTAMBAH
				if($row2 == $array_term_copy[$i]["term"]){
					$array_term_copy[$i]["df"]++;
					$ketemu++;
				}
			}
			///JIKA TIDAK TERDAPAT PADA ARRAY MAKA AKAN MENAMBAHKAN KATA-KATA BARU DALAM ARRAY
			if($ketemu==0){
				array_push($array_term_copy,array(
					"term" => $row2,
					"df" => 1,
					"cf" => 0,
					"idf" => 0,
					"icf" => 0
				));
			}
			$count= count($array_term_copy); 
			
		}

		///MEGHITUNG IDF ICF
		for ($i=0; $i < $count; $i++) {
			$array_term_copy[$i]["idf"]=log10(1+(5/$array_term_copy[$i]["df"]));
			$array_term_copy[$i]["icf"]=log10(1+(1+2)/(1+$array_term_copy[$i]["cf"]));
        }
        
		///COPY ARRAY
		$count= count($array_term_copy); 
		for($i=0; $i< $count ;$i++){
			$array_kata[] = array(
				"term" => $array_term_copy[$i]["term"],
				"tf" => 0,
				"tfidficf" => 0,
				"tfidficf_pangkat" =>0
			);
		}
		
		//AMBIL SEMUA KALIMAT LATIH
		$array_kalimat_latih = $this->model_latih->get_all_data_latih();
		$array_data_latih=$array_kata;
		$array_data_uji=$array_kata;
        ///MENGHITUNG TF DALAM KALIMAT DATA UJI
        
		for ($i=0; $i < $count; $i++) { 
			foreach ($array_uji_term as $row2) {

				if($array_data_uji[$i]["term"]==$row2){
					$array_data_uji[$i]["tf"]++;
				}
			}
			
		}
		
		foreach ($array_kalimat_latih as $kalimat) {
			$array_latih_term = $this->model_latih->get_all_term($kalimat);
			$hasil_eucledian=0;
			$hasil_cosine=0;
			$jumlah_pangkat_data_latih = 0;
			$jumlah_pangkat_data_uji = 0;
			$jumlah_perkalian_datauji_x_datalatih=0;
			///MENGHITUNG TF DALAM SETIAP KALIMAT DATA LATIH
			for ($i=0; $i < $count; $i++) { 
				foreach ($array_latih_term as $row2) {
					if($array_data_latih[$i]["term"]==$row2){
						$array_data_latih[$i]["tf"]++;
					}
				}
			}
			
			for ($i=0; $i < $count; $i++) { 
				$hitung_eucledian=0;
				///MENGHITUNG TF IDF ICF
				$array_data_latih[$i]["tfidficf"]=$array_data_latih[$i]["tf"]*$array_term_copy[$i]["idf"]*$array_term_copy[$i]["icf"];
				$array_data_uji[$i]["tfidficf"]=$array_data_uji[$i]["tf"]*$array_term_copy[$i]["idf"]*$array_term_copy[$i]["icf"];
				
				///COSINE SIMILARITY
				$array_data_latih[$i]["tfidficf_pangkat"]=pow($array_data_latih[$i]["tfidficf"],2);
                $array_data_uji[$i]["tfidficf_pangkat"]=pow($array_data_uji[$i]["tfidficf"],2);
				$datauji_x_datatraining = $array_data_latih[$i]["tfidficf"] * $array_data_uji[$i]["tfidficf"];
                $jumlah_perkalian_datauji_x_datalatih=$jumlah_perkalian_datauji_x_datalatih+$datauji_x_datatraining;
                
                $jumlah_pangkat_data_latih=$jumlah_pangkat_data_latih+$array_data_latih[$i]["tfidficf_pangkat"];
				$jumlah_pangkat_data_uji=$jumlah_pangkat_data_uji+$array_data_uji[$i]["tfidficf_pangkat"];
                
            }
            
			$jumlah_pangkat_data_latih=sqrt($jumlah_pangkat_data_latih);
            $jumlah_pangkat_data_uji=sqrt($jumlah_pangkat_data_uji);
            
			$hasil_cosine=$jumlah_perkalian_datauji_x_datalatih / ($jumlah_pangkat_data_latih * $jumlah_pangkat_data_uji);				
			array_push($array_hasil_cosine,$hasil_cosine);

			$array_data_latih=$array_kata;
		}

		///SORTING DARI TERBESAR KE TERKECIL
		arsort($array_hasil_cosine);

		///MENGAMBIL ARRAY SEBANYAK K
		$hasil_knn_cosine=array_slice($array_hasil_cosine,0,$k,true);
        //die(var_dump($hasil_knn_cosine));
		

		foreach ($hasil_knn_cosine as $key => $value) {

			if($value!=0){
				$hasil_cosine_knn[]=array(
					"eucledian" => $value,
					"label" => $array_label_latih[$key]["jenis_sentimen"]
				);
			}
			else{
				$hasil_cosine_knn[]=array(
					"eucledian" => $value,
					"label" => NULL
				);
			}
			
        }
        
		$prediksi_cosine = $this->model_latih->prediksi($hasil_cosine_knn);
		
		$hasil_prediksi[]=array(
			"jenis_sentimen" => $prediksi_cosine
		);
		
		
		return $hasil_prediksi;
	}
    public function insert_datauji($k){
		$array_uji= $this->get_all_data_uji();
		$data_model=$this->tfidficf_latih();
		$data_uji=$this->get_all_data();
		$i=0;
        foreach ($array_uji as $row_uji) {
			$hasil_prediksi=NULL;
            $data = $this->tfidficf($row_uji,$k,$data_model);
            //die(var_dump($data));

            $data_update = array(
                'id_dataset_twitter' => $data_uji[$i]["id_dataset_twitter"],
                'ID' => $data_uji[$i]["ID"],
                'url' => $data_uji[$i]["url"],
                'datetime' => $data_uji[$i]["datetime"],
                'text' => $data_uji[$i]["text"],
                'user_id' => $data_uji[$i]["user_id"],
                'usernameTweet' => $data_uji[$i]["usernameTweet"],
				'jenis_sentimen' => $data[0]["jenis_sentimen"],
				'id_admin' => $this->session->userdata['ses_admin']['id']
            );
            $this->db->replace('sa_dataset_twitter',$data_update);
            $i++;

		}
		
		
	}

}
