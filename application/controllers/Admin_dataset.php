<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_dataset extends CI_Controller
{
    public function __construct()
    {
    parent::__construct();
        $this->load->library('csvimport');
    }
    public function index(){
        $data['content'] = NULL;
        $sess = $this->session->userdata('ses_admin');
        $data['page']= "dataset";
        $this->load->view('admin/layout',$data);
    }
    public function ajax_list(){
    	$list=$this->model_dataset->get_datatables();
    	$data=array();
    	$no= $_POST['start'];
    	foreach ($list as $admin_dataset) {
    		$no++;
    		$row = array();
            $row[]=$no;
            $row[]=$admin_dataset->jenis_sentimen;
            $row[]=$admin_dataset->kategori;
            $row[]=$admin_dataset->text;

    		$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_obat('."'".$admin_dataset->id_dataset."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
                    <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$admin_dataset->id_dataset."'".')"><i class="glyphicon glyphicon-trash"></i></a>';

            $data[] = $row;
    	}
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_dataset->count_all(),
                        "recordsFiltered" => $this->model_dataset->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    public function ajax_edit($id)
    {
        $data = $this->model_dataset->get_by_id($id);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'obat_nama' => $this->input->post('namaObat'),
                'obat_stok' => $this->input->post('stokObat'),
                'obat_kemasan' => $this->input->post('kemasanObat'),
            );
        $insert = $this->model_dataset->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
                'obat_nama' => $this->input->post('namaObat'),
                'obat_stok' => $this->input->post('stokObat'),
                'obat_kemasan' => $this->input->post('kemasanObat'),
            );
        $this->model_dataset->update(array('id_dataset' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id)
    {
        $this->model_dataset->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
    public function delete(){
        $this->model_dataset->delete_all();
        echo json_encode(array("status" => TRUE));
    }
    function import()
	{
        $file_data = $this->csvimport->get_array($_FILES["csv_file"]["tmp_name"]);
        //die(var_dump($file_data));
		foreach($file_data as $row)
		{
			$data[] = array(
                'text'			=>	$row["text"],
                'jenis_sentimen'    => $row["Sentimen"],
                'kategori'      => $row["kategori"]
			);
		}
		$this->model_dataset->insert($data);
	} 
}