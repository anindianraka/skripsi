<?php 
if (!$this->session->userdata('ses_admin')){
    redirect(base_url());
}
?>
<!DOCTYPE html>
<html>
<?php $this->load->view('admin/templates/head.php'); ?>
<body class="hold-transition skin-green-light sidebar-mini">
    <div class="wrapper">
        <?php $this->load->view('admin/templates/header.php');?>
        <?php 
        if ($this->session->userdata['ses_admin']['akses']=='admin') {
            $this->load->view('admin/templates/sidebar_admin.php');
        }
        else{
            $this->load->view('admin/templates/sidebar_user.php');
        }
        ?>
        <?php $this->load->view('admin/pages/'.$page.'.php',$content); ?>  
    </div>
    <?php
    if($page == 'dashboard')
    {
        $this->load->view('admin/templates/script_dashboard.php');
    }
    else if($page == 'twitter_pre'){
        $this->load->view('admin/templates/script_hasil.php');
    }
    else
    {
        $this->load->view('admin/templates/script.php');
    }
    ?>
    <?php  ?>  

    
</body>
</html>

