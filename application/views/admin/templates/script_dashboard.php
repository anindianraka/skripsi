<script src="<?php echo base_url('assets/admin/bower_components/jquery/dist/jquery.min.js'); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('assets/admin/bower_components/jquery-ui/jquery-ui.min.js');?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url('assets/admin/plugins/iCheck/all.css');?>">
<!-- Morris.js charts -->
<script src="<?php echo base_url('assets/admin/bower_components/raphael/raphael.min.js');?>"></script>
<script src="<?php echo base_url('assets/admin/bower_components/morris.js/morris.min.js');?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js');?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>"></script>
<script src="<?php echo base_url('assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/admin/bower_components/jquery-knob/dist/jquery.knob.min.js');?>"></script>
<!-- daterangepicker --><!-- 
<script src="assets/admin/bower_components/moment/min/moment.min.js"></script>
<script src="assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script> -->
<!-- datepicker -->
<!-- <script src="assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> -->
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url('assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js');?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/admin/bower_components/fastclick/lib/fastclick.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/admin/dist/js/adminlte.min.js');?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="assets/admin/dist/js/pages/dashboard.js"></script>
AdminLTE for demo purposes
<script src="assets/admin/dist/js/demo.js"></script> -->
<!-- DataTables -->
<script src="<?php echo base_url('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js');?>"></script>
<script src="<?php echo base_url('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
    var data_latih_pos = $("#jumlah-data-latih-positif").text();
    var data_latih_neg = $("#jumlah-data-latih-negatif").text();
    var data_latih = $("#jumlah-data-latih").text();
    var data_uji = $("#jumlah-data-uji").text();
    //DONUT CHART
    var donut = new Morris.Donut({
      element: 'sales-chart',
      resize: true,
      colors: ["#00a65a", "#f56954"],
      data: [{
          label: "Sentimen Positif",
          value: data_latih_pos
        },
        {
          label: "Sentimen Negatif",
          value: data_latih_neg
        }
      ],
      hideHover: 'auto'
    });
    //DONUT CHART
    var donut = new Morris.Donut({
      element: 'sales',
      resize: true,
      colors: ['#4CB1CF', '#F0AD4E'],
      data: [{
          label: "Data Latih",
          value: data_latih
        },
        {
          label: "Data Uji",
          value: data_uji
        }
      ],
      hideHover: 'auto'
    });
});
</script>