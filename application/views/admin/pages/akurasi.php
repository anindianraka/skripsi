<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Kata Dasar
            <small>Analisis Sentimen</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo base_url('admin_katadasar') ?>">Kata Dasar</a></li>
            <!-- <li class="active">Data Obat</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Data Obat</h3>
                        <br>
                        <br />
                        <button class="btn btn-success bg-green" onclick="akurasi()"><i class="glyphicon glyphicon-plus"></i>
                            Tambah Kata Dasar</button>
                        <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i>
                            Segarkan</button>
                        <br />
                        <form action="<?=base_url()?>admin_akurasi/insert_datauji" method="post">
                            <div class="row">
                                <!-- /.col -->
                                <div class="col-xs-4">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Uji Akurasi</button>
                                </div>
                                <!-- /.col -->
                            </div>
                        </form>
                        <div class="row">
                            <!-- /.col -->
                            <div class="col-xs-4">
                                <button class="btn btn-primary btn-block btn-flat" id="btn_akurasi">Uji Akurasi</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tweet</th>
                                    <th>Sentimen Asli</th>
                                    <th>P Pos</th>
                                    <th>P Neg</th>
                                    <th>Hasil Analisis</th>
                                    <th>Hasil</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row" id="matrix">
            <div class="col-xs-12">
                <div class="callout callout-success">
                    <p class="text-center"><strong>Confusion Matrix</strong></p>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Jumlah Data Uji :<span id="total-datauji"></span></td>

                                    <td><strong>Sentimen Hasil Analisis Positif</strong></td>
                                    <td><strong>Sentimen Hasil Analisis Negatif</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>Sentimen Asli Positif</strong></td>
                                    <td>True Positives =<span id="true-positives"></span></td>
                                    <td>False Negatives = <span id="false-negatives"></span></td>
                                </tr>
                                <tr>
                                    <td><strong>Sentimen Asli Negatif</strong></td>
                                    <td>False Positives =<span id="false-positives"></span></td>
                                    <td>True Negatives = <span id="true-negatives"></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="callout callout-success">
                    <table class="borderless">
                        <tbody>
                            <tr>
                                <th width="310">
                                    <strong>Akurasi (TP+TN/Total Data Uji)</strong>
                                </th>
                                <th width="20">
                                    =
                                </th>
                                <th>
                                    <span id="akurasi"></span>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="callout callout-success">
                    <table class="borderless">
                        <tbody>
                            <tr>
                                <th width="310">
                                    <strong>Error Rate (1 - Akurasi)</strong>
                                </th>
                                <th width="20">
                                    =
                                </th>
                                <th>
                                    <span id="error-rate"></span>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="callout callout-success">
                    <table class="borderless">
                        <tbody>
                            <tr>
                                <th width="310">
                                    <strong>Positive Predictive Value / Presisi (TP/TP+FP)</strong>
                                </th>
                                <th width="20">
                                    =
                                </th>
                                <th>
                                    <span id="ppv"></span>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="callout callout-success">
                    <table class="borderless">
                        <tbody>
                            <tr>
                                <th width="310">
                                    <strong>Negative Predictive Value (TN/TN+FN)</strong>
                                </th>
                                <th width="20">
                                    =
                                </th>
                                <th>
                                    <span id="npv"></span>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="callout callout-success">
                    <table class="borderless">
                        <tbody>
                            <tr>
                                <th width="310">
                                    <strong>Sensitivity / Recall (TP/TP+FN)</strong>
                                </th>
                                <th width="20">
                                    =
                                </th>
                                <th>
                                    <span id="sensitivity"></span>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="callout callout-success">
                    <table class="borderless">
                        <tbody>
                            <tr>
                                <th width="310">
                                    <strong>Specificity (TN/TN+FP)</strong>
                                </th>
                                <th width="20">
                                    =
                                </th>
                                <th>
                                    <span id="specificity"></span>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

<!-- modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Person Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id" />
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Obat</label>
                            <div class="col-md-9">
                                <input name="namaObat" placeholder="Nama Obat" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Stok Obat</label>
                            <div class="col-md-9">
                                <input name="stokObat" placeholder="Stok Obat" class="form-control" type="number">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kemasan Obat</label>
                            <div class="col-md-9">
                                <select name="kemasanObat" class="form-control">
                                    <option value="Kaplet">Kaplet</option>
                                    <option value="Pack">Pack</option>
                                    <option value="Toples">Toples</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->