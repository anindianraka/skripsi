<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_stopwords extends CI_Controller
{
    public function index(){
        $data['content'] = NULL;
        $sess = $this->session->userdata('ses_admin');
        $data['page']= "stopword";
        $this->load->view('admin/layout',$data);
    }
    public function ajax_list(){
    	$list=$this->model_stopwords->get_datatables();
    	$data=array();
    	$no= $_POST['start'];
    	foreach ($list as $admin_stopwords) {
    		$no++;
    		$row = array();
            $row[]=$no;
    		$row[]=$admin_stopwords->kata_stopwords;

    		$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_obat('."'".$admin_stopwords->id_stopwords."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$admin_stopwords->id_stopwords."'".')"><i class="glyphicon glyphicon-trash"></i> Delete </a>';

            $data[] = $row;
    	}
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_stopwords->count_all(),
                        "recordsFiltered" => $this->model_stopwords->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    public function ajax_edit($id)
    {
        $data = $this->model_stopwords->get_by_id($id);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'obat_nama' => $this->input->post('namaObat'),
                'obat_stok' => $this->input->post('stokObat'),
                'obat_kemasan' => $this->input->post('kemasanObat'),
            );
        $insert = $this->model_stopwords->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
                'obat_nama' => $this->input->post('namaObat'),
                'obat_stok' => $this->input->post('stokObat'),
                'obat_kemasan' => $this->input->post('kemasanObat'),
            );
        $this->model_stopwords->update(array('id_stopwords' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id)
    {
        $this->model_stopwords->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
}
