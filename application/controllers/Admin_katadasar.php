<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_katadasar extends CI_Controller
{
    public function index(){
        $data['content'] = NULL;
        $sess = $this->session->userdata('ses_admin');
        $data['page']= "katadasar";
        $this->load->view('admin/layout',$data);
    }
    public function ajax_list(){
    	$list=$this->model_katadasar->get_datatables();
    	$data=array();
    	$no= $_POST['start'];
    	foreach ($list as $admin_katadasar) {
    		$no++;
    		$row = array();
            $row[]=$no;
    		$row[]=$admin_katadasar->kata_katadasar;

    		$row[] =    '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_obat('."'".$admin_katadasar->id_katadasar."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                        <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$admin_katadasar->id_katadasar."'".')"><i class="glyphicon glyphicon-trash"></i> Delete </a>';

            $data[] = $row;
    	}
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->model_katadasar->count_all(),
                        "recordsFiltered" => $this->model_katadasar->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    public function ajax_edit($id)
    {
        $data = $this->model_katadasar->get_by_id($id);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $data = array(
                'obat_nama' => $this->input->post('namaObat'),
                'obat_stok' => $this->input->post('stokObat'),
                'obat_kemasan' => $this->input->post('kemasanObat'),
            );
        $insert = $this->model_katadasar->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $data = array(
                'obat_nama' => $this->input->post('namaObat'),
                'obat_stok' => $this->input->post('stokObat'),
                'obat_kemasan' => $this->input->post('kemasanObat'),
            );
        $this->model_katadasar->update(array('id_katadasar' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id)
    {
        $this->model_katadasar->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
}
