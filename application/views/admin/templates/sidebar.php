<aside class="main-sidebar ">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="assets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo ($this->session->userdata['ses_admin']['nama']); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu tree" data-widget="tree">
        <li <?php echo ( $this->uri->segment(1) == 'admin_dashboard' ? 'class="active treeview"': ''); ?>><a href="<?php echo base_url('admin_dashboard'); ?>"><i class="fa fa-home"></i> <span>Beranda</span></a></li>
        <li <?php echo ( $this->uri->segment(1) == 'admin_dataset' ? 'class="active treeview"': ''); ?>><a href="<?php echo base_url('admin_dataset'); ?>"><i class="fa fa-file"></i> <span>Dataset</span></a></li>
        <!-- <li <?php echo ( $this->uri->segment(1) == 'admin_stopwords' ? 'class="active treeview"': ''); ?>><a href="<?php echo base_url('admin_stopwords'); ?>"><i class="fa fa-file-word-o"></i> <span>Stop Words</span></a></li> -->
        <li <?php echo ( $this->uri->segment(1) == 'admin_preprocessing' ? 'class="active treeview"': ''); ?>><a href="<?php echo base_url('admin_preprocessing'); ?>"><i class="fa fa-refresh"></i> <span>Preprocessing</span></a></li>
        <li <?php echo ( $this->uri->segment(1) == 'admin_latih' ? 'class="active treeview"': ''); ?>><a href="<?php echo base_url('admin_latih'); ?>"><i class="fa fa-files-o"></i> <span>Latih</span></a></li>
        <li class="treeview  <?php if($this->uri->segment(1) == 'admin_akurasi_eucledian' ||  $this->uri->segment(1) == 'admin_akurasi_cosine' || $this->uri->segment(1) == 'admin_akurasi_jaccord'){echo 'active';}?>">
          <a href="#">
            <i class="fa fa-bar-chart"></i> <span>Akurasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo ( $this->uri->segment(1) == 'admin_akurasi_eucledian' ? 'class="active"': ''); ?>><a href="<?php echo base_url('admin_akurasi_eucledian'); ?>"><i class="fa fa-circle-o"></i> Eucledian Distance</a></li>
            <li <?php echo ( $this->uri->segment(1) == 'admin_akurasi_cosine' ? 'class="active"': ''); ?>><a href="<?php echo base_url('admin_akurasi_cosine'); ?>"><i class="fa fa-circle-o"></i> Cosine Similarity</a></li>
            <li <?php echo ( $this->uri->segment(1) == 'admin_akurasi_jaccord' ? 'class="active"': ''); ?>><a href="<?php echo base_url('admin_akurasi_jaccord'); ?>"><i class="fa fa-circle-o"></i> Jaccord Coefficience</a></li>
          </ul>
        </li>
        <li class="treeview  <?php if($this->uri->segment(1) == 'admin_twitter' ||  $this->uri->segment(1) == 'admin_twitter_pre' || $this->uri->segment(1) == 'admin_twitter_hasil'){echo 'active';}?>">
          <a href="#">
            <i class="fa fa-fw fa-twitter"></i> <span>Twitter</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?php echo ( $this->uri->segment(1) == 'admin_twitter' ? 'class="active"': ''); ?>><a href="<?php echo base_url('admin_twitter'); ?>"><i class="fa fa-circle-o"></i> Tweet</a></li>
            <li <?php echo ( $this->uri->segment(1) == 'admin_twitter_pre' ? 'class="active"': ''); ?>><a href="<?php echo base_url('admin_twitter_pre'); ?>"><i class="fa fa-circle-o"></i> Preprocessing</a></li>
            <li <?php echo ( $this->uri->segment(1) == 'admin_twitter_hasil' ? 'class="active"': ''); ?>><a href="<?php echo base_url('admin_twitter_hasil'); ?>"><i class="fa fa-circle-o"></i> Hasil</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>