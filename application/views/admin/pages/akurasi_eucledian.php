<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Data Akurasi Eucledian
            <small>Analisis Sentimen</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-home"></i> Beranda</a></li>
            <li><a href="<?php echo base_url('admin_akurasi_eucledian') ?>">Akurasi Eucledian</a></li>
            <!-- <li class="active">Data Obat</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="row">
                            <div class="col-xs-4">
                            <button class="btn btn-success bg-green " id="btn_akurasi_eucledian"><i class="glyphicon glyphicon-stats"></i> Uji
                                    Akurasi</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tweet</th>
                                    <th>Label</th>
                                    <th>Prediksi</th>
                                    <th>Hasil</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <div class="row" id="matrix_eucledian">
            <div class="col-xs-12">
                <div class="callout callout-success">
                    <p class="text-center"><strong>Confusion Matrix</strong></p>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Jumlah Data Uji :<span id="total-datauji"></span></td>

                                    <td><strong>Sentimen Hasil Analisis Positif</strong></td>
                                    <td><strong>Sentimen Hasil Analisis Negatif</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>Sentimen Asli Positif</strong></td>
                                    <td>True Positives =<span id="true-positives"></span></td>
                                    <td>False Negatives = <span id="false-negatives"></span></td>
                                </tr>
                                <tr>
                                    <td><strong>Sentimen Asli Negatif</strong></td>
                                    <td>False Positives =<span id="false-positives"></span></td>
                                    <td>True Negatives = <span id="true-negatives"></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="callout callout-success">
                    <table class="borderless">
                        <tbody>
                            <tr>
                                <th width="310">
                                    <strong>Akurasi (TP+TN/Total Data Uji)</strong>
                                </th>
                                <th width="20">
                                    =
                                </th>
                                <th>
                                    <span id="akurasi"></span>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="callout callout-success">
                    <table class="borderless">
                        <tbody>
                            <tr>
                                <th width="310">
                                    <strong>Precision (TP/TP+FP)</strong>
                                </th>
                                <th width="20">
                                    =
                                </th>
                                <th>
                                    <span id="ppv"></span>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="callout callout-success">
                    <table class="borderless">
                        <tbody>
                            <tr>
                                <th width="310">
                                    <strong>Recall (TP/TP+FN)</strong>
                                </th>
                                <th width="20">
                                    =
                                </th>
                                <th>
                                    <span id="sensitivity"></span>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>