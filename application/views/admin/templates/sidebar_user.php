<aside class="main-sidebar ">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="assets/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>
                    <?php echo ($this->session->userdata['ses_admin']['nama']); ?>
                </p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu tree" data-widget="tree">
            <li class="treeview  <?php if($this->uri->segment(1) == 'admin_twitter' ||  $this->uri->segment(1) == 'admin_twitter_pre' || $this->uri->segment(1) == 'admin_twitter_hasil'|| $this->uri->segment(1) == 'admin_twitter_hasil_positif'|| $this->uri->segment(1) == 'admin_twitter_hasil_negatif'){echo 'active';}?>">
                <a href="#">
                    <i class="fa fa-fw fa-twitter"></i> <span>Twitter</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php echo ( $this->uri->segment(1) == 'admin_twitter' ? 'class="active"': ''); ?>><a href="<?php echo base_url('admin_twitter'); ?>"><i
                                class="fa fa-circle-o"></i> Tweet</a></li>
                    <li <?php echo ( $this->uri->segment(1) == 'admin_twitter_pre' ? 'class="active"': ''); ?>><a href="<?php echo base_url('admin_twitter_pre'); ?>"><i
                                class="fa fa-circle-o"></i> Preprocessing</a></li>
                    <li <?php echo ( $this->uri->segment(1) == 'admin_twitter_hasil_negatif' ||$this->uri->segment(1)
                        == 'admin_twitter_hasil' || $this->uri->segment(1) == 'admin_twitter_hasil_positif' ?
                        'class="active"': ''); ?>><a href="<?php echo base_url('admin_twitter_hasil'); ?>"><i class="fa fa-circle-o"></i>
                            Hasil</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>